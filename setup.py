from distutils.core import setup

setup(name='sklearntools',
      version='0.1',
      description='Tools for sklearn models',
      author='Jason Rudy',
      author_email='jcrudy@gmail.com',
      url='https://gitlab.com/jcrudy/sklearntools',
      packages=['sklearntools'],
      package_data = {'': ['resources/*']},
     )